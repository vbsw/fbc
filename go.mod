module gitlab.com/vbsw/fbc

go 1.17

require (
	gitlab.com/vbsw/golib/check/v2 v2.0.3
	gitlab.com/vbsw/golib/iter v1.0.0
	gitlab.com/vbsw/golib/osargs v1.0.0
)
